#! /bin/bash
DIR_NAME=$APP_NAME-$APP_VERSION
FILE_NAME=$APP_NAME-$APP_VERSION.tar.gz

DOWNLOAD_URL=https://github.com/Kitware/CMake/releases/download/v$APP_VERSION/$FILE_NAME

# Make a dir for the app, to keep it a bit more organized
mkdir $APP_NAME
cd $APP_NAME

# Fetch the source code
wget $DOWNLOAD_URL

# Extract the package with the source code
tar -xvf $FILE_NAME

cd $DIR_NAME

./bootstrap --system-curl --no-system-expat --no-system-jsoncpp --no-system-zlib --system-bzip2 --no-system-liblzma --no-system-libarchive --no-system-librhash --no-system-libuv --no-qt-gui --prefix=${INSTALL_PATH}

gmake -j ${CORES}

make install